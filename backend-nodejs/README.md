# Backend nodejs

This is a simple nodejs backend.
The only thing this backend does is send a
standard response on a GET on /api

## Getting started

Run

```bash
yarn install
```

## scripts

Start the backend

```bash
yarn start
```

Run all the tests:

```bash
yarn test
```

Build the backend:

```bash
yarn build
```
