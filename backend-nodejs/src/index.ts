import dotenv from 'dotenv'
import { app } from './app'

dotenv.config()

const port = process.env.PORT ?? 8000

app.listen(port, () => {
    console.log('\n')
    console.log('✓    server is running')
    console.log(`     env:      ${process.env.NODE_ENV ?? 'unknown'}`)
    console.log(`     address:  http://localhost:${port}`)
    console.log('\nPress CTRL-C to stop\n')
})
