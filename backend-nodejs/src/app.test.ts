import request from 'supertest'
import { app } from './app'

describe('Testing backend-nodejs', () => {

    it('Should make a succesfull call', async () => {
        await request(app)
            .get('/api')
            .expect('Content-Type', /json/)
            .expect('Content-Length', '51')
            .expect(200)
    })
    it('Should make a call to get prometheus metrics', async () => {
        await request(app)
            .get('/metrics')
            .expect('Content-Type', 'text/plain')
            .expect(200)
    })
    it('Should get a 418 when using a non existing path', async () => {
        await request(app)
            .get('/nonexisting')
            .expect('Content-Type', /json/)
            .expect('Content-Length', '75')
            .expect(418)
    })
})
