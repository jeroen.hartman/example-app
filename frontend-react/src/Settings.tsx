import { Link, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
import { useEffect, useState } from 'react'
import './App.css'
import { Setting } from './types/backend-python-db'

const Settings = () => {

  const [responseFromPython, setResponseFromPython] = useState<Setting[] | null>(null)

  useEffect(() => {
    fetch(`./python-api/v1/settings`)
      .then(async response => {
        if (!response.ok)
          return
        setResponseFromPython(await response.json())
      })
  }, [])
  return (
    <div className="App">
      <p>Settings from python api stored in database:</p>
      {responseFromPython &&
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="Settings from python api stored in database">
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell align="right">Name</TableCell>
                <TableCell align="right">Value</TableCell>
                <TableCell align="right">Regex</TableCell>
                <TableCell align="right">Last modified</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {responseFromPython.map((setting) => (
                <TableRow
                  key={setting.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">{setting.id}</TableCell>
                  <TableCell align="right">{setting.name}</TableCell>
                  <TableCell align="right">{setting.value}</TableCell>
                  <TableCell align="right">{setting.regex}</TableCell>
                  <TableCell align="right">{setting.lastModified}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      }
      <p>
        <Link href="../">Home</Link>
      </p>
    </div>
  )
}

export default Settings
