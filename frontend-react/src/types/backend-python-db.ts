/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

/** HTTPValidationError */
export interface HTTPValidationError {
  /** Detail */
  detail?: ValidationError[];
}

/** Setting */
export interface Setting {
  /** Name of the setting */
  name: string;
  /** Value of the setting */
  value: string;
  /** Description of the setting */
  description: string;
  /** Regex of the setting */
  regex: string;
  /** The unique id of the setting */
  id: string;
  /**
   * The unique uuid of the setting
   * @format uuid
   */
  uuid: string;
  /**
   * Date of last modification
   * @format date-time
   */
  lastModified?: string;
}

/** SettingCreate */
export interface SettingCreate {
  /** Name of the setting */
  name: string;
  /** Value of the setting */
  value: string;
  /** Description of the setting */
  description: string;
  /** Regex of the setting */
  regex: string;
}

/** SettingUpdate */
export interface SettingUpdate {
  /** Name of the setting */
  name?: string;
  /** Value of the setting */
  value?: string;
  /** Description of the setting */
  description?: string;
  /** Regex of the setting */
  regex?: string;
}

/** ValidationError */
export interface ValidationError {
  /** Location */
  loc: any[];
  /** Message */
  msg: string;
  /** Error Type */
  type: string;
}
