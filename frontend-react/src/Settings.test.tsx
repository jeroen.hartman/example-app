import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import App from './App'
import { Setting } from './types/backend-python-db'
import Settings from './Settings'


const mockSettings: Setting[] = [
  {
    "id": "SET-0001",
    "uuid": "f82334d3-df70-4383-96ee-c8a2aaabf6c9",
    "name": "fontColor",
    "value": "#000",
    "description": "Color of the font",
    "regex": "",
    "lastModified": "2023-04-24T14:25:53.869232"
  },
  {
    "id": "SET-0002",
    "uuid": "3e8cc880-0003-45dd-be7b-617779266085",
    "name": "backgroundColor",
    "value": "#FFF",
    "description": "The background color",
    "regex": ".*",
    "lastModified": "2023-04-24T14:41:58.692732"
  }
]

test('renders settings page', async () => {

  jest.spyOn(global, "fetch").mockImplementation(
    jest.fn(
      () => Promise.resolve({
        ok: true,
        json: () => Promise.resolve(mockSettings),
      }),
    ) as jest.Mock)

  render(<Settings />)
  const linkElement = screen.getByText(/Settings from python api stored in database/i)
  expect(linkElement).toBeInTheDocument()

  await waitFor(() => expect(screen.getByText('SET-0001')).toBeVisible())

})

test('renders settings page when api is not available', async () => {

  jest.spyOn(global, "fetch").mockImplementation(
    jest.fn(
      () => Promise.resolve({
        ok: false
      }),
    ) as jest.Mock)

  render(<Settings />)

  expect(await screen.findByText(/Settings from python api stored in database/i)).toBeInTheDocument()

})