import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import App from './App'


test('renders learn react link', async () => {

  jest.spyOn(global, "fetch").mockImplementation(
    jest.fn(
      () => Promise.resolve({
        json: () => Promise.resolve({ "description": "This is the mock response from the api" }),
      }),
    ) as jest.Mock)

  render(<App />)
  const linkElement = screen.getByText(/learn react/i)
  expect(linkElement).toBeInTheDocument()

  await waitFor(() => expect(screen.getByText('This is the mock response from the api')).toBeVisible())

})
