# backend-python-db

## Todo

- creat tables
- add gitlab-ci
- add tests
- Add page in frontend
- add e2etest

This is an example of a backend api which uses a database

>> python 3.11 and poetry need to be installed

```bash
cd backend-python-db
```

```bash
poetry install
```

urls:

<https://xxllnc.localtest.me/python-api/docs>
<https://xxllnc.localtest.me/python-api/redoc>

connect to local db:
host: postgres.localtest.me
databse: settings
user: settings
pw: settings123

## Migrations

To create a new migration, first define the table in `app.db.models` and then run:

```bash
docker-compose run --rm backend-python-db alembic revision --autogenerate -m "initial migration"
```

This will generate an update script for Alembic. Double-check if it does what
it should, adjust as needed, and add it to your commit. The upgrade/downgrade
is described in the `upgrade` and `downgrade` methods of a newly created file
in `backend-python-db/app/alembic/versions`.

For more information see
[Alembic's official documentation](https://alembic.sqlalchemy.org/en/latest/tutorial.html#create-a-migration-script).

### Run new migrations

```bash
docker-compose run --rm backend-python-db alembic upgrade head
docker-compose run --rm backend-python-db alembic downgrade -1
```

### Add example data

```bash
docker-compose run --rm backend-python-db python3 app/initial_data.py
```

## Testing

```bash
docker-compose run --rm backend-python-db pytest -vvsx
```
