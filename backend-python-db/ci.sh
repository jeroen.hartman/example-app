#! /bin/bash

set -e

fix_errors=0
junit_dir=""

while getopts "fo:" opt; do
  case "${opt}" in
    f)
      fix_errors=1
      ;;
    o)
      junit_dir="${OPTARG}"
      ;;
    \?)
      echo "Usage: $0 [-f] [-o=testfile_output_dir]"
      ;;
  esac
done

if [ "$fix_errors" -ne 1 ]; then
    ISORT_ARGS="--check-only"
    BLACK_ARGS="--check"
fi

PYTEST_ARGS="-p no:cacheprovider --verbose --cov-branch --cov=."
if [ -n "$junit_dir" ]; then
    PYTEST_ARGS="$PYTEST_ARGS --junitxml=$junit_dir/junit.xml --cov-report=xml:$junit_dir/coverage.xml"
else
    PYTEST_ARGS="$PYTEST_ARGS --cov-report=term-missing"
fi

# Code formatting
echo "Run isort"
isort $ISORT_ARGS app
echo "Run black"
black $BLACK_ARGS app

# Linter`
echo "Linting: Run ruff"
ruff check .

# Type checker
echo "Use mypy to check types"
mypy -p app

# Check dependencies for known security issues
# safety check

# Test suite (unit tests)
echo "run unit tests"
pytest $PYTEST_ARGS