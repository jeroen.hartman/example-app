from fastapi import APIRouter
from fastapi.responses import FileResponse

router = r = APIRouter()


@r.get("/.well-known/security.txt", response_class=FileResponse)
async def main():
    return FileResponse("/app/security.txt", media_type="text/plain")
