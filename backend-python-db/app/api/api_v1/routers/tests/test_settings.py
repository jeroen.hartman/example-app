import pytest
from app.api.api_v1.routers.tests.test_mock_data_settings import add_mock_data_to_db
from app.core.config import BASE_PATH_V1
from fastapi import status as http_status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from typing import Final

url: Final = f"{BASE_PATH_V1}/settings"


# ---------------------------------------------
# GET requests tests
# ---------------------------------------------
def test_get_all_settings(client: TestClient, test_db: Session):
    mock_data = add_mock_data_to_db(test_db)

    response = client.get(url)
    assert response.status_code == http_status.HTTP_200_OK

    headers = response.headers
    assert len(headers) == 2
    assert headers["content-type"] == "application/json"

    response_body = response.json()
    assert len(response_body) == len(mock_data)


def _test_get_setting_by_id(client: TestClient, test_db: Session, id, mock_data):
    response = client.get(f"{url}/{id}")

    assert response.status_code == http_status.HTTP_200_OK
    response_body = response.json()

    # Check if id is generated and two fields.
    assert response_body["id"] is not None
    assert response_body["name"] == mock_data.setting1.name
    assert response_body["description"] == mock_data.setting1.description


def test_get_setting_by_id(client: TestClient, test_db: Session):
    mock_data = add_mock_data_to_db(test_db)

    _test_get_setting_by_id(client, test_db, mock_data.setting1.id, mock_data)
    _test_get_setting_by_id(client, test_db, mock_data.setting1.uuid, mock_data)


@pytest.mark.parametrize("id", ["1234", 1234, "39a0ca90-5ca8-11ec-bf63-0242ac130002"])
def test_get_setting_by_unknown_id(client: TestClient, test_db: Session, id):
    add_mock_data_to_db(test_db)

    response = client.get(f"{url}/{id}")

    assert response.status_code == http_status.HTTP_404_NOT_FOUND


# ---------------------------------------------
# POST requests tests
# ---------------------------------------------
def test_create_new_setting(client: TestClient, test_db: Session):
    new_setting = {
        "name": "new setting",
        "value": "new value",
        "description": "This setting is added by the test",
        "regex": ".*",
    }
    response = client.post(url, json=new_setting)

    assert response.status_code == 200
    response_body = response.json()
    assert response_body["id"] is not None
    assert response_body["uuid"] is not None
    assert response_body["name"] == "new setting"


# ---------------------------------------------
# PUT requests tests
# ---------------------------------------------
def test_update_setting_by_id(client: TestClient, test_db: Session):
    mock_data = add_mock_data_to_db(test_db)

    update_setting = {"name": "new name"}
    response = client.put(f"{url}/{mock_data.setting1.id}", json=update_setting)

    assert response.status_code == 200

    response_body = response.json()
    assert response_body["name"] == "new name"
    assert response_body["value"] == mock_data.setting1.value


def test_update_setting_by_uuid(client: TestClient, test_db: Session):
    mock_data = add_mock_data_to_db(test_db)

    update_setting = {"name": "new name", "value": "new value"}
    response = client.put(f"{url}/{mock_data.setting2.uuid}", json=update_setting)

    assert response.status_code == 200

    response_body = response.json()
    assert response_body["name"] == "new name"
    assert response_body["value"] == "new value"


# ---------------------------------------------
# DELETE requests tests
# ---------------------------------------------
def _test_delete_setting_by_id(client: TestClient, test_db: Session, id):
    response = client.delete(f"{url}/{id}")

    assert response.status_code == 200


def test_delete_setting(client: TestClient, test_db: Session):
    mock_data = add_mock_data_to_db(test_db)

    _test_delete_setting_by_id(client, test_db, mock_data.setting1.id)
    _test_delete_setting_by_id(client, test_db, mock_data.setting2.uuid)

    response = client.get(url)
    assert response.status_code == http_status.HTTP_200_OK

    assert len(response.json()) == 0
