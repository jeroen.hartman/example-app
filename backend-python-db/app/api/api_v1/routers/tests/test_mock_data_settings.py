#!/usr/bin/env python3

import datetime
from app.db import models
from collections import namedtuple
from sqlalchemy.orm import Session

# Declaring namedtuple() as return value
MockData = namedtuple(
    "MockData",
    ["setting1", "setting2"],
)


def add_mock_data_to_db(db: Session) -> MockData:
    setting1 = models.Setting(
        name="organisatienaam",
        description="Naam van de organisatie",
        regex=".*",
        value="Xxllnc",
        last_modified=datetime.datetime(2021, 5, 11, 00, 1, 00),
    )
    setting2 = models.Setting(
        name="urlAfsluitenKnop",
        description="De standaard url die wordt gebruikt voor de "
        "afsluitknop van het formulier. Deze waarde kan in het formulier "
        "worden overschreven door in het formulier een instelling "
        "urlAfsluitenKnop toe te voegen",
        regex=".*",
        value="https://xxllnc.nl/",
        last_modified=datetime.datetime(2021, 5, 11, 00, 1, 00),
    )

    db.add(setting1)
    db.add(setting2)

    db.flush()

    return MockData(
        setting1,
        setting2,
    )
