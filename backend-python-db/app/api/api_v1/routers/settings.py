import uuid
from app.db.session import get_read_db, get_write_db
from app.domain import settings
from app.schemas import setting_schemas
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

router = r = APIRouter()


@r.get(
    "/settings/{id}",
    response_model=setting_schemas.Setting,
    responses={404: {"description": "setting not found"}},
)
def get_setting_by_id_or_uuid(id: uuid.UUID | str, db=Depends(get_read_db)):
    return settings.get(db=db, id=id)


@r.get("/settings", response_model=list[setting_schemas.Setting])
def get_settings_all_settings(db: Session = Depends(get_read_db)):
    return settings.get_all(db=db)


@r.post(
    "/settings",
    response_model=setting_schemas.Setting,
    responses={409: {"description": "Conflict. setting not updated."}},
)
def create_new_setting(setting: setting_schemas.SettingCreate, db=Depends(get_write_db)):
    return settings.create(db=db, obj_in=setting)


@r.put(
    "/settings/{id}",
    response_model=setting_schemas.Setting,
    responses={
        404: {"description": "setting not found"},
        409: {"description": "Conflict. setting not updated."},
    },
)
def update_setting_on_id_or_uuid(
    id: uuid.UUID | str,
    setting: setting_schemas.SettingUpdate,
    db=Depends(get_write_db),
):
    return settings.update(db=db, id=id, obj_in=setting)


@r.delete(
    "/settings/{id}",
    response_model=setting_schemas.Setting,
    responses={
        404: {"description": "setting not found"},
        409: {"description": "Conflict. setting not updated."},
    },
)
def delete_setting(id: uuid.UUID | str, db=Depends(get_write_db)):
    return settings.delete(db=db, id=id)
