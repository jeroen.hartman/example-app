from app.core.types import SemanticError, SemanticErrorType
from fastapi import status as http_status
from fastapi.responses import JSONResponse


class semantic_error_exception_handler:
    def __call__(self, exception: SemanticError) -> JSONResponse:
        """Handles a SemanticError raised in the domain conform CTECO-97"""

        status_code_map = {
            SemanticErrorType.already_deleted: http_status.HTTP_409_CONFLICT,
            SemanticErrorType.invalid_input: http_status.HTTP_400_BAD_REQUEST,
            SemanticErrorType.not_found: http_status.HTTP_404_NOT_FOUND,
            SemanticErrorType.not_unique: http_status.HTTP_409_CONFLICT,
            SemanticErrorType.conflict: http_status.HTTP_409_CONFLICT,
        }

        message = exception.msg if isinstance(exception.msg, str) else "unknow error"
        if (
            isinstance(exception.msg, list)
            and len(exception.msg) > 0
            and "msg" in exception.msg[0]
        ):
            message = exception.msg[0]["msg"]

        return JSONResponse(
            status_code=status_code_map[exception.type],
            content={"detail": [exception.__dict__], "message": message},
        )
