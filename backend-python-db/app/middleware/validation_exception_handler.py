import copy
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import Any


class validation_exception_handler:
    def __call__(self, exception) -> JSONResponse:
        """Convert default format to desired format where 'loc' is not a tuple but
        a dict: loc: { 'source': 'body', 'field': 'name'}. See CTECO-94"""

        errors: list[dict[str, Any]] = copy.deepcopy(exception.errors())
        for i, e in enumerate(exception.errors()):
            loc = e["loc"]
            if type(loc) == tuple and len(loc) == 2:
                new_loc: dict[str, Any] = {"source": loc[0], "field": loc[1]}
                errors[i]["loc"] = new_loc

        """
        jsonable_encoder already gives a formated error in this format:
        {
        "detail": [
            {
            "loc": {
                "source": "body",
                "field": "contact"
            },
            "msg": "field required",
            "type": "value_error.missing"
            }
        ]
        }
        """

        return JSONResponse(
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": errors}),
        )
