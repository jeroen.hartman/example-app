import sqlalchemy as sa
import uuid
from app.core.types import ProcessError
from app.db.models import Setting
from app.schemas import setting_schemas
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from typing import Sequence


class CRUDSettings:
    def get(self, db: Session, id: uuid.UUID | str) -> Setting:
        if isinstance(id, uuid.UUID):
            query = sa.select(Setting).where(Setting.uuid == id)
        else:
            query = sa.select(Setting).where(Setting.id == id)

        result = db.execute(query).scalars().first()

        if not result:
            raise ProcessError(
                loc={"source": "path", "field": "id"},
                msg="No setting found with {id}",
                status_code=http_status.HTTP_404_NOT_FOUND,
            )

        return result

    def get_all(self, db: Session) -> Sequence[Setting]:
        return db.execute(sa.select(Setting)).scalars().all()

    def create(self, db: Session, obj_in: setting_schemas.SettingCreate) -> Setting:
        obj_in_data = obj_in.dict(exclude_unset=True)
        db_obj = Setting(**obj_in_data)

        try:
            db.add(db_obj)
            db.flush()
            db.commit()
        except sa.exc.IntegrityError as e:
            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg="Error adding setting",
                status_code=http_status.HTTP_409_CONFLICT,
            ) from e
        db.refresh(db_obj)
        return db_obj

    def update(
        self, db: Session, id: uuid.UUID | str, obj_in: setting_schemas.SettingUpdate
    ) -> Setting:
        update_data = obj_in.dict(exclude_unset=True)

        #  get original Setting from DB
        db_obj_to_update = self.get(db=db, id=id)
        obj_data = jsonable_encoder(db_obj_to_update)

        # Check all fields and update the one's that where changed
        for field in obj_data:
            if field in update_data:
                setattr(db_obj_to_update, field, update_data[field])
        try:
            db.add(db_obj_to_update)
            db.commit()
        except sa.exc.IntegrityError as e:
            raise ProcessError(
                loc={"source": "body", "field": "name"},
                msg="Unable to update Setting",
                status_code=http_status.HTTP_409_CONFLICT,
            ) from e
        db.refresh(db_obj_to_update)

        return db_obj_to_update

    def delete(self, db: Session, id: uuid.UUID | str) -> Setting:
        obj = self.get(db, id)

        db.delete(obj)
        db.commit()
        return obj


settings = CRUDSettings()
