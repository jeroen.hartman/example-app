import enum
from fastapi import HTTPException
from typing import Any


class SemanticErrorType(str, enum.Enum):
    already_deleted = "semantic_error.already_deleted"
    invalid_input = "semantic_error.invalid_input"
    not_found = "semantic_error.not_found"
    not_unique = "semantic_error.not_unique"
    conflict = "semantic_error.conflict"


class SemanticError(Exception):
    """Exception raised for semantic errors"""

    def __init__(
        self,
        loc: dict,
        msg: str | list[dict[str, Any]],
        type: SemanticErrorType,
    ):
        self.loc = loc
        self.msg = msg
        self.type = type
        super().__init__(self.msg)

    def detail(self):
        return {
            "loc": self.loc,
            "msg": self.msg,
            "type": self.type,
        }


class ProcessError(HTTPException):
    def __init__(
        self,
        loc: dict,
        msg: str | list[dict[str, Any]],
        status_code: int,
    ):
        self.loc = loc
        self.msg = msg
        super().__init__(status_code)

    def detail(self):
        return {
            "loc": self.loc,
            "msg": self.msg,
        }
