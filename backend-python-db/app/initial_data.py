#!/usr/bin/env python3

"""Module for creating initial data for the database"""  # pragma: no cover

from app.db import models  # pragma: no cover
from app.db.session import write_session
from loguru import logger
from sqlalchemy import text  # pragma: no cover
from sqlalchemy.orm import Session  # pragma: no cover


def truncate_tables() -> None:  # pragma: no cover
    """Truncate tables"""

    database: Session = write_session()
    database.execute(text('truncate table "setting" cascade'))
    database.commit()


def restart_sequences_with_1() -> None:  # pragma: no cover
    """Restart the sequences with 1"""

    db: Session = write_session()
    db.execute(text('ALTER SEQUENCE "setting_sid_seq" RESTART WITH 1'))
    db.commit()


def create_initial_data() -> None:  # pragma: no cover
    """Creates initial data for database"""

    database: Session = write_session()

    setting1 = models.Setting(
        name="backgroundColor",
        value="#FFF",
        description="Background color",
    )
    setting2 = models.Setting(
        name="fontColor",
        value="#000",
        description="Color of the font",
    )

    database.add(setting1)
    database.add(setting2)

    database.flush()
    database.commit()


if __name__ == "__main__":  # pragma: no cover
    logger.info("First remove any existing data")
    truncate_tables()
    logger.info("Restart the sequences with 1")
    restart_sequences_with_1()
    logger.info("Creating initial data")
    create_initial_data()
    logger.info("Initial data created")
