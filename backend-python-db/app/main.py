import logging
import os
import sys
import uvicorn
from app.api.api_v1.routers import security, settings
from app.core.config import BASE_PATH, BASE_PATH_V1, PROJECT_NAME
from app.core.logging import InterceptHandler
from app.core.types import SemanticError
from app.middleware.check_content_type import CheckContentTypeMiddleware
from app.middleware.semantic_error_exception_handler import semantic_error_exception_handler
from app.middleware.validation_exception_handler import validation_exception_handler
from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from loguru import logger
from typing import Final

API_DESCRIPTION: Final = """
backend-python-db can be used to make, edit, add and delete settings.
"""

app = FastAPI(
    title=PROJECT_NAME,
    description=API_DESCRIPTION,
    version="0.0.1",
    contact={
        "name": "Xxllnc developer website",
        "url": "https://gitlab.com/xxllnc/cloud/xcp-public-charts/example-app",
    },
    license_info={
        "name": "Xxllnc: Licensed under the EUPL-1.2-or-later",
        "url": "https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12",
    },
    docs_url=f"{BASE_PATH}/docs",
    redoc_url=f"{BASE_PATH}/redoc",
    openapi_url=BASE_PATH,
)


@app.middleware("http")
async def log_middle(request: Request, call_next):
    logger.trace(f"{request.method} {request.url}")
    logger.trace("Params:")
    for name, value in request.path_params.items():
        logger.trace(f"\t{name}: {value}")
    logger.trace("Headers:")
    for name, value in request.headers.items():
        logger.trace(f"\t{name}: {value}")

    response = await call_next(request)
    return response


app.add_middleware(CheckContentTypeMiddleware)


@app.exception_handler(RequestValidationError)
async def validation_error_handler(request: Request, exc: RequestValidationError):
    hander = validation_exception_handler()
    return hander(exc)


@app.exception_handler(SemanticError)
async def semantic_error_handler(request: Request, exc: SemanticError):
    hander = semantic_error_exception_handler()
    return hander(exc)


# Routers

app.include_router(settings.router, prefix=BASE_PATH_V1, tags=["Settings"])
app.include_router(security.router, prefix=BASE_PATH_V1, tags=["security.txt"])

# Get log level.
LOG_LEVEL = logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO"))
LOGGERS = ("uvicorn.asgi", "uvicorn.access", "uvicorn.error")

# Configure root logger.
logging.root.handlers = [InterceptHandler()]
logging.root.setLevel(LOG_LEVEL)

# Remove every other logger's handlers and propagate to root logger
for name in LOGGERS:
    logging_logger = logging.getLogger(name)
    logging_logger.handlers = []
    logging_logger.setLevel(LOG_LEVEL)
    logging_logger.propagate = True

# Set format
logger.configure(handlers=[{"sink": sys.stdout, "level": LOG_LEVEL}])


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", reload=True, port=8885)
