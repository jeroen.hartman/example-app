#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

  CREATE DATABASE settings;

  \c settings
  -- Enable UUID field type
  CREATE EXTENSION "uuid-ossp";
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  SELECT 'CREATE USER settings SUPERUSER PASSWORD ''settings123'''
  WHERE NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'settings')\gexec
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  GRANT ALL PRIVILEGES ON DATABASE settings TO settings;
EOSQL
