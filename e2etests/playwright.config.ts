import { defineConfig, devices } from '@playwright/test'

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  testDir: './tests',
  timeout: 30 * 1000,
  globalTimeout: process.env.CI ? 1200 * 1000 : 600 * 1000,
  expect: {
    timeout: 5000
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: 1,
  workers: process.env.CI ? 1 : undefined,
  reporter: [
    ['list'],
    ['line'],
    ['html', { open: 'never', outputFolder: './tests/test-reports/' }],
  ],
  use: {
    baseURL: 'https://cloud-example-development.ext.xcp-np1.np.xxllnc.nl',
    locale: 'nl-NL',
    video: 'on',
    // video: process.env.CI ? 'retain-on-failure' : 'on',
    screenshot: 'only-on-failure',
    actionTimeout: 0,
    trace: 'retain-on-failure',
    ignoreHTTPSErrors: true,
  },
  outputDir: './tests/test-screenshots/',
  projects: [
    {
      name: 'e2e tests on chromium',
      use: {
        ...devices['Desktop Chrome']
      }
    },
    {
      name: 'e2e tests on Firefox',
      use: {
        ...devices['Desktop Firefox']
      }
    }
  ]
})
