import { test, expect } from '@playwright/test'

test('test if react page is shown and reponse from api is on screen', async ({ page }) => {

  await page.goto('https://cloud-example-development.ext.xcp-np1.np.xxllnc.nl/')

  await page.waitForLoadState('domcontentloaded')

  await expect(page.getByText('Response from backend-nodejs: This is the response from the api')).toBeVisible()

})

