# Helm Charts

This directory contains all the helm charts that are used to deploy
this example app to the Xxllnc Cloud Platform (XCP)

The following basic directory structure is used:

``` bash
.
├── deployments
│   ├── development
│   │   └── values.yaml
│   └── production
│       └── values.yaml
├── templates
│   ├── backend-nodejs
│   │   ├── ...
│   ├── frontend-react
│   │   ├── ...
│   ├── _helpers.tpl
│   └── ingress.yaml
├── Chart.yaml
└── values.yaml
```

In this example there is one `Chart.yaml` with two templates.
one template for backend-nodejs and one for frontend-react,.

The default values are found in `values.yaml` in the root of the
helm directory.
The default values are overridden at the deployment by the values in deployments

There is some redundant code in the Helm Chart, this is done to make
the example easier to read.

## Manual deployment

With this Helm Chart it is possible to deploy the app to your own namespace.

> All `helm` and `kubectl` commands should be run from within the helm directory. So first `cd helm`

***

### Preparation

> To start we assume you already have access to AWS and there is a project in Rancher that you can use.

Before we can start the deployment there are a few things that need to be prepared.

1. Install [kubectl](https://kubernetes.io/docs/tasks/tools/) and [Helm](https://helm.sh/docs/intro/install/) on your local machine
2. Connect VPN to AWS, See <https://xxllnc.atlassian.net/wiki/spaces/CP/pages/966755844386/user+aws+client+vpn>
3. Download kubeconfig from Rancher. The kubeconfig can be downloaded on the cluster home page (top right) in Rancher and move the file to `~/.kube/config file`. Verify with a kubectl command e.g. `kubectl version`

***

### Verify before deployment

Before the deployment is done the Helm Chart is verified. This is done by running a helm lint and helm template and inspect the output. Change the working directory to the helm directory `cd helm`

```bash
helm lint .
```

Expected output:

``` bash
==> Linting .
1 chart(s) linted, 0 chart(s) failed
```

Second step is to template the Chart and see which output is generated

```bash
helm template .
```

> Running this command doesn't give any output. This is because default the apps are disabled in the deployment.

Now create a local new values.yaml to enable the apps:

```bash
mkdir -p deployments/local  && touch deployments/local/values.yaml
```

Open `deployments/local/values.yaml` and copy the following contents

```yaml
backendNodejs:
  enabled: true

frontendReact:
  enabled: true
```

Now run the following command to see the helm output based on `deployments/local/values.yaml`

```bash
helm template -f deployments/local/values.yaml .
```

Now there is output because both apps are enabled.
Helm combines the default `values.yaml` with `deployments/local/values.yaml`.
With `deployments/local/values.yaml` default values can be overridden. If a value is
not overridden then the default value from `values.yaml` is used

Examining the output. It may be easy to write the output to a file with this command:

```bash
helm template -f deployments/local/values.yaml . > deployChart.yaml
```

***

### Helm upgrade/install

When all previous steps are completed it is now time to deploy the Helm Chart

```bash
helm upgrade --install --debug --namespace <your-name-space> -f deployments/local/values.yaml deployment-name .
```

When the deploy was successful the app can be open on the wildcard domain.

<your-name-space.ext.xcp-np1.np.xxllnc.nl>

#### Helm uninstall

When you are finished the last step is to clean-up. Use the following command to remove the deployment:

```bash
helm uninstall --namespace <your-name-space> deployment-name
```
